package govAutomation

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	machineBasePath          = "catalog-service/api/consumer"
	entitledCatalogItemsPath = machineBasePath + "/entitledCatalogItems"
	requestsPath             = machineBasePath + "/requests"
	resourceViews            = "resourceViews"
	resourcesPath            = machineBasePath + "/resources"
	machinePath              = resourcesPath + "/types/Infrastructure.Machine"
)

// Client is a govAutomation Client used to query the vRealize automation API
type Client struct {
	Client   *http.Client `json:"-"`
	Host     string       `json:"-"`
	Username string       `json:"username"`
	Password string       `json:"password"`
	Tenant   string       `json:"tenant"`
	Token    string       `json:"-"`
}

// tokenReq
type tokenReq struct {
	Token   string `json:"id"`
	Expires string `json:"expires"`
	Errors  []struct {
		Code          int         `json:"code"`
		Message       string      `json:"message"`
		SystemMessage string      `json:"systemMessage"`
		MoreInfoURL   interface{} `json:"moreInfoUrl"`
	} `json:"errors"`
}

// NewClient returns a govAutomation client
func NewClient(host, username, password, tenant string, httpClient *http.Client) (client *Client, err error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	client = &Client{
		Username: username,
		Password: password,
		Tenant:   tenant,
		Client:   httpClient,
		Host:     host,
	}

	err = client.GetToken()
	if err != nil {
		return nil, err
	}

	return client, nil
}

// getToken creates a json body containing the c.Username, c.Password and c.Tenant date from the client struct.
// vRealize automation generates a http bearer token using this data.
func (c *Client) GetToken() (err error) {
	jsonData, err := json.Marshal(c)
	if err != nil {
		return err
	}

	body := bytes.NewReader(jsonData)
	u := fmt.Sprintf("%s/identity/api/tokens", c.Host)
	req, err := http.NewRequest("POST", u, body)
	if err != nil {
		return err
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.Client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var id tokenReq
	err = json.Unmarshal(b, &id)
	if err != nil {
		return err
	}

	for _, v := range id.Errors {
		if v.Code == 90135 {
			return errors.New("Unable to authenticate")
		}
	}
	c.Token = id.Token
	return nil
}

// getToken creates a json body containing the c.Username, c.Password and c.Tenant date from the client struct.
// vRealize automation generates a http bearer token using this data.
func (c *Client) DeleteToken() (err error) {
	jsonData, err := json.Marshal(c)
	if err != nil {
		return err
	}

	body := bytes.NewReader(jsonData)
	u := fmt.Sprintf("%s/identity/api/tokens/%s", c.Host, c.Token)
	req, err := http.NewRequest("DELETE", u, body)
	if err != nil {
		return err
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	b, err := c.doRequest(req)
	if err != nil {
		return err
	}

	fmt.Print(string(b))
	// var id tokenReq
	// err = json.Unmarshal(b, &id)
	// if err != nil {
	// 	return err
	// }

	// for _, v := range id.Errors {
	// 	if v.Code == 90135 {
	// 		return errors.New("Unable to authenticate")
	// 	}
	// }
	// c.Token = id.Token
	return nil
}

func (c *Client) doRequest(req *http.Request) ([]byte, error) {
	req.Header.Set("Authorization", "Bearer "+c.Token)
	resp, err := c.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if 200 != resp.StatusCode {
		return nil, fmt.Errorf("%s", body)
	}
	return body, nil
}
