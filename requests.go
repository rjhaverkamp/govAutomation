package govAutomation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// GetAllRequests returns the last 20 catalog requests from vRealize
func (c *Client) GetAllRequests() (requests *Requests, err error) {
	u := fmt.Sprintf("%s/%s%s", c.Host, requestsPath, "?limit=20&$orderby=requestNumber%20desc")
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}
	b, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(b, &requests)
	if err != nil {
		return nil, err
	}
	return requests, nil
}

// GetRequest gets a request identified by ID
func (c *Client) GetRequest(ID string) (request *Request, err error) {
	u := fmt.Sprintf("%s/%s/%s", c.Host, requestsPath, ID)
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}
	b, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(b, &request)
	if err != nil {
		return nil, err
	}
	c.GetRequestView(ID)
	return request, nil
}

// GetRequestView gets a request identified by ID
func (c *Client) GetRequestView(ID string) (request *Request, err error) {
	u := fmt.Sprintf("%s/%s/%s/%s", c.Host, requestsPath, ID, resourceViews)
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}
	b, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(b))
	err = json.Unmarshal(b, &request)
	if err != nil {
		return nil, err
	}
	return request, nil
}

// Requests is a collection of multiple requests
type Requests struct {
	Content []struct {
		Type          string      `json:"@type"`
		ID            string      `json:"id"`
		Version       int         `json:"version"`
		RequestNumber int         `json:"requestNumber"`
		State         string      `json:"state"`
		RequestedFor  string      `json:"requestedFor"`
		RequestedBy   string      `json:"requestedBy"`
		DateCreated   time.Time   `json:"dateCreated"`
		LastUpdated   time.Time   `json:"lastUpdated"`
		DateSubmitted time.Time   `json:"dateSubmitted"`
		DateApproved  interface{} `json:"dateApproved"`
		DateCompleted interface{} `json:"dateCompleted"`
		Quote         struct {
		} `json:"quote"`
		RequestCompletion struct {
			RequestCompletionState string `json:"requestCompletionState"`
			CompletionDetails      string `json:"completionDetails"`
		} `json:"requestCompletion"`
		RetriesRemaining         int    `json:"retriesRemaining"`
		RequestedItemName        string `json:"requestedItemName"`
		RequestedItemDescription string `json:"requestedItemDescription"`
		StateName                string `json:"stateName"`
		Phase                    string `json:"phase"`
		ExecutionStatus          string `json:"executionStatus"`
		WaitingStatus            string `json:"waitingStatus"`
		ApprovalStatus           string `json:"approvalStatus"`
	} `json:"content"`
}

// Request is a standalone request
type Request struct {
	Type                   string    `json:"@type"`
	ID                     string    `json:"id"`
	Version                int       `json:"version"`
	RequestNumber          int       `json:"requestNumber"`
	State                  string    `json:"state"`
	RequestedFor           string    `json:"requestedFor"`
	RequestedBy            string    `json:"requestedBy"`
	RequestorEntitlementID string    `json:"requestorEntitlementId"`
	DateCreated            time.Time `json:"dateCreated"`
	LastUpdated            time.Time `json:"lastUpdated"`
	DateSubmitted          time.Time `json:"dateSubmitted"`
	DateCompleted          time.Time `json:"dateCompleted"`
	RequestCompletion      struct {
		RequestCompletionState string      `json:"requestCompletionState"`
		CompletionDetails      string      `json:"completionDetails"`
		ResourceBindingIds     interface{} `json:"resourceBindingIds"`
	} `json:"requestCompletion"`
	RetriesRemaining         int    `json:"retriesRemaining"`
	RequestedItemName        string `json:"requestedItemName"`
	RequestedItemDescription string `json:"requestedItemDescription"`
	StateName                string `json:"stateName"`
	Phase                    string `json:"phase"`
	ExecutionStatus          string `json:"executionStatus"`
	WaitingStatus            string `json:"waitingStatus"`
	ApprovalStatus           string `json:"approvalStatus"`
}
